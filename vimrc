set nocompatible              " be iMproved

set background=dark
set backspace=indent,eol,start
set history=1000
set number
set t_Co=256
set mouse=a
let g:airline_powerline_fonts=1
set guifont=Inconsolata\ for\ Powerline:h18

" convert tabs to spaces, use 4 spaces (in tab jump and shift)
set ruler
set incsearch
set showcmd
set cursorline

set tabstop=2
set shiftwidth=2
set softtabstop=2
set laststatus=2
set expandtab
set smartindent

set scrolloff=5
set nohid
set title
set encoding=utf-8
set fileencoding=utf-8
set pastetoggle=<F2>
set exrc

set colorcolumn=120

set backupcopy=yes

" --- folding---
set foldmethod=syntax
set nofoldenable
set foldnestmax=3           " deepest fold is 3 levels

filetype off

set rtp+=~/.vim/bundle/Vundle.vim/
call vundle#begin()
" Vundle is a plugin manager for Vim
Plugin 'VundleVim/Vundle.vim'
" File browsing side panel
Plugin 'scrooloose/nerdtree'
" Git plugin for Nerdtree
Plugin 'Xuyuanp/nerdtree-git-plugin'
" Advanced commenting plugin
Plugin 'scrooloose/nerdcommenter'
" Solarized color scheme for Vim
Plugin 'altercation/vim-colors-solarized'
" Git management plugin
Plugin 'tpope/vim-fugitive'
" Run external commands in Vim panels
Plugin 'tpope/vim-dispatch'
" JavaScript support
Plugin 'pangloss/vim-javascript'
" Visually indent guides in code
Plugin 'nathanaelkane/vim-indent-guides'
" Autocompletion in insert mode for quotes/brackets
Plugin 'Raimondi/delimitMate'
" Show file modifications in Git on the side
Plugin 'airblade/vim-gitgutter'
" Generate tags for javascript
Plugin 'elentok/supertagger'
" Show a diff when recovering a swap file
Plugin 'chrisbra/Recover.vim'
" Move inside camel case variables
Plugin 'bkad/CamelCaseMotion'
" Vue Support
Plugin 'posva/vim-vue'
" YAML Support
Plugin 'avakhov/vim-yaml'
" Jump between tags in HTML
Plugin 'tmhedberg/matchit'
" Show tags and structure for classes
Plugin 'majutsushi/tagbar'
" Support navigation between vim and tmux
Plugin 'christoomey/vim-tmux-navigator'
" Show the history of undos
Plugin 'mbbill/undotree'
" Manage clipboard history
" Plugin 'vim-scripts/YankRing.vim'
" Post gists to github
Plugin 'mattn/gist-vim'
" Syntax hlighliting and status
" Plugin 'scrooloose/syntastic'
" Vue support for syntastic
" Plugin 'sekel/vim-vue-syntastic'
" Solidity support
Plugin 'tomlion/vim-solidity'
" Alternate between test file and source easily
Plugin 'compactcode/alternate.vim'
" Navigation between brackets
Plugin 'tpope/vim-unimpaired'
" Add ability to zoom a buffer to full screen
Plugin 'vim-scripts/ZoomWin'
" JSON Support
Plugin 'elzr/vim-json'
" Supprt for running unit tests
Plugin 'janko-m/vim-test'
" Quick search with ack/ag
Plugin 'mileszs/ack.vim'
" Bottom quick jump panel
Plugin 'ctrlpvim/ctrlp.vim'
" Support for airline in the status buffer
Plugin 'bling/vim-airline'
" Syntax verification for jS
Plugin 'dense-analysis/ale'
call vundle#end()

" I hate spaces in end of lines or tabs anywhere
match Error /\t\|\s\+$/

imap <C-c> <CR><Esc>O
inoremap <silent> <Esc> <C-O>:stopinsert<CR>

autocmd BufWritePre * :%s/\s\+$//e
autocmd BufEnter * let &titlestring = ' ' . expand("%:t")

" Configuration for nerdtree
let NERDTreeQuitOnOpen = 1
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let NERDTreeAutoDeleteBuffer = 1
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1

map <C-n> :NERDTreeToggle<CR>
map <C-f> :NERDTreeFind<CR>

" Configuration for ack
let g:ackprg = 'ag --nogroup --nocolor --column'

" Configuration for syntastic
let g:syntastic_solidity_checkers = ['solhint']
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_javascript_eslint_exe = 'npm run lint --'

" Configuration for ale
let g:ale_linters = {
      \   'javascript': ['standard'],
      \}
let g:ale_fixers = {'javascript': ['standard']}
let g:ale_lint_on_save = 1
let g:ale_fix_on_save = 1

" Set json files to open with the vim-json plugin
autocmd FileType json setlocal foldmethod=syntax

" Setup vim-fugitive shortcuts
nnoremap <space>ga :Git add %:p<CR><CR>
nnoremap <space>gs :Gstatus<CR>
nnoremap <space>gc :Gcommit -v -q<CR>
nnoremap <space>gt :Gcommit -v -q %:p<CR>
nnoremap <space>gd :Gdiff<CR>
nnoremap <space>ge :Gedit<CR>
nnoremap <space>gr :Gread<CR>
nnoremap <space>gw :Gwrite<CR><CR>
nnoremap <space>gl :silent! Glog<CR>:bot copen<CR>
nnoremap <space>gp :Ggrep<Space>
nnoremap <space>gm :Gmove<Space>
nnoremap <space>gb :Git branch<Space>
nnoremap <space>go :Git checkout<Space>
nnoremap <space>gps :Dispatch git push<CR>
nnoremap <space>gpl :Dispatch git pull<CR>

" Configuration for vim-test
" these "Ctrl mappings" work well when Caps Lock is mapped to Ctrl
nmap <silent> t<C-n> :TestNearest<CR> " t Ctrl+n
nmap <silent> t<C-f> :TestFile<CR>    " t Ctrl+f
nmap <silent> t<C-s> :TestSuite<CR>   " t Ctrl+s
nmap <silent> t<C-l> :TestLast<CR>    " t Ctrl+l
nmap <silent> t<C-g> :TestVisit<CR>   " t Ctrl+g

" Configuration for ctrl-p
let g:ctrlp_working_path_mode='ra'
" let g:ctrlp_extensions = ['tag']
let g:ctrlp_user_command = 'ag %s -i --nocolor --nogroup --hidden
      \ --ignore .git
      \ --ignore .DS_Store
      \ --ignore node_modules
      \ --ignore review
      \ -g ""'
map <C-o> :CtrlPBuffer<CR>

" Automatically reload vimrc when saved
augroup reload_vimrc " {
    autocmd!
    autocmd BufWritePost $MYVIMRC source $MYVIMRC
augroup END " }

nmap <silent> ,/ :nohlsearch<CR>

" Trick if forgot to sudo
cmap w!! %!sudo tee > /dev/null %
au BufRead * normal zR

" Change the colorscheme settings
colorscheme solarized
highlight SignColumn ctermbg=Black

" Set a separate swap file directory
set directory=~/.vim/swapfiles/

" Left over settings
filetype plugin indent on
syntax on
