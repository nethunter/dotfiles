#!/bin/bash

DTFE=~/.dotfiles

cd $DTFE
git submodule init
git submodule update
cd zprezto
git submodule init
git submodule update
cd $DTFE
ln -s dircolors-solarised ~/.dir_colors
ln -s gitconfig ~/.gitconfig
ln -s vimrc ~/.vimrc
ln -s vim ~/.vim
ln -s tmux.conf ~/.tmux.conf
ln -s tmux-powerlinerc ~/.tmux-powerlinerc

setopt EXTENDED_GLOB
for rcfile in "${ZDOTDIR:-$HOME}"/.zprezto/runcoms/^README.md(.N); do
    ln -s "$rcfile" "${ZDOTDIR:-$HOME}/.${rcfile:t}"
done

